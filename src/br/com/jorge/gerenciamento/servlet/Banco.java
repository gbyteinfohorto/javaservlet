package br.com.jorge.gerenciamento.servlet;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Banco {

	private static List<Empresa> lista = new ArrayList<>();
	private static Integer chaveSequencial = 1;
	
	static {
		
		Empresa empresa = new Empresa();
		empresa.setId(chaveSequencial++);
		empresa.setNome("HST Card Tecnology");
		
		Empresa empresa2 = new Empresa();
		empresa2.setId(chaveSequencial++);
		empresa2.setNome("Banco Bradesco");
		
		lista.add(empresa);
		lista.add(empresa2);
	}
	
	public void adiciona(Empresa empresa) {
		empresa.setId(Banco.chaveSequencial++);
		Banco.lista.add(empresa);
		
		System.out.println("Page Banco - Adicionado empresa ao Banco: Nome= " + empresa.getNome() + " | Data=" + empresa.getDataAbertura() + " | Id= " +empresa.getId());
		//RECUPERANDO VALOR SUBMETIDO COM empresa.getNome()		
	}
	
	public List<Empresa> getEmpresas(){
		return Banco.lista;
	}
	
	public void removeEmpresa(Integer id) {
		
		Iterator<Empresa> it = lista.iterator();
		while(it.hasNext()){
			Empresa emp = it.next();
			if(emp.getId() == id) {
					System.out.println("Page Banco - Removendo do Banco Empresa com: id= " + id);
					//REMOVENDO VALOR DO BANCO PELO ID - MOSTRNDO NO CONSOLE ANTES DE APAGAR
					it.remove();
			}
		}
			
	}

	public Empresa buscaEmpresaPelaId(Integer id) {
		for (Empresa empresa : lista) {
			if(empresa.getId() == id) {
				return empresa;
			}
		}
		return null;
	}
	
}
