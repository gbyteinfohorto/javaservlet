package br.com.jorge.gerenciamento.servlet;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class NovaEmpresaServlet
 */

@WebServlet("/novaEmpresa")
public class NovaEmpresaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
				 //doPost ou doGet obriga o subimit utilizar o metodo descrito
			throws ServletException, IOException {
		System.out.println("\nPage Servlet - Cadastrando Nova Empresa");
		 
		String nomeEmpresa = request.getParameter("nome");//RECEBE O PARABMETRO COM GETPARAMETER
		String paramDataEmpresa = request.getParameter("data");//RECEBE O PARABMETRO COM GETPARAMETER
		
		Date dataAbertura = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");//SIMPLIFICANDO O FORMATO DATE COM A CLASS SIMPLEDATEFORMAT()
			dataAbertura = sdf.parse(paramDataEmpresa);//TRANSFORMANDO OBJETO DATE PARA O TIPO SITRING
		} catch (ParseException e) {
			throw new ServletException(e + " => FORMATO DA DATA PADR�O => DD/MM/AAAA");
		}
		
		Empresa empresa = new Empresa();//CHAMA O MODELO E INSTANCIA
		empresa.setNome(nomeEmpresa);// POPULANDO O MODELO COM O NOME
		empresa.setDataAbertura(dataAbertura);// POPULANDO O MODELO COM A DATA
		
		Banco banco = new Banco();//CHAMA O SIMULADOR DE BANCO E INSTANCIA
		banco.adiciona(empresa);//BANCO INSTANCIADO, ADICINA EMPRESA AO BANCO COM METODO ADICIONA
		
		request.setAttribute("empresa", empresa.getNome());
		response.sendRedirect("listaEmpresas");
		
		//CHAMA O JSP - novaEmpresaCriada.jsp
		//RequestDispatcher rd = request.getRequestDispatcher("/listaEmpresas");
		//request.setAttribute("empresa", empresa.getNome());
		//rd.forward(request, response);
	}

}
