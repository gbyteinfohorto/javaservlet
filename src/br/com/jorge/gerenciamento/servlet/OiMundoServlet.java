package br.com.jorge.gerenciamento.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//MAPEAMENTO 1 - COM ANOTA��O WEBSERVLET FUNCIONANDO
//@WebServlet(urlPatterns = "/Mapeamento1")
public class OiMundoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public OiMundoServlet() {
		System.out.println("Chamado pelo metodo publico");
	}
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		
		PrintWriter out = resp.getWriter();
		out.println("<html>");
		out.println("<body>");
		out.println("<h3>Primeiro Servlet completo, Jorge Mira.</h3>");
		out.println("<h3>link com  = ANOTACOES.desativado(/Mapeamento1) e XML.ativado(/mapeamento2)</h3>");
		out.println("</body>");
		out.println("</html>");
		
		System.out.println("link com = ANOTACOES.desativado(/Mapeamento1) e XML.ativado(/mapeamento2)");
		
	}
}
