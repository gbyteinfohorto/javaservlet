package br.com.jorge.gerenciamento.servlet;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/alterarEmpresa")
public class AlterarEmpresaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		System.out.println("\nPage Servlet - Alterando Empresa Cadastrada");
		 
		String nomeEmpresa = request.getParameter("nome");//RECEBE O PARABMETRO COM GETPARAMETER
		String paramDataEmpresa = request.getParameter("data");//RECEBE O PARABMETRO COM GETPARAMETER
		
		String paramId = request.getParameter("id");//RECEBE O PARABMETRO COM GETPARAMETER
		Integer id = Integer.valueOf(paramId);
		
		Date dataAbertura = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");//SIMPLIFICANDO O FORMATO DATE COM A CLASS SIMPLEDATEFORMAT()
			dataAbertura = sdf.parse(paramDataEmpresa);//TRANSFORMANDO OBJETO DATE PARA O TIPO SITRING
		} catch (ParseException e) {
			throw new ServletException(e + " => FORMATO DA DATA PADR�O => DD/MM/AAAA");
		}
		
		System.out.println("Page Servlet - Aleterado empresa do id: " + id);
		
		//SIMULANDO O BANCO DE DADOS RELACIONAL COMO SQL
		Banco banco = new Banco();
		Empresa empresa = banco.buscaEmpresaPelaId(id);
		empresa.setNome(nomeEmpresa);
		empresa.setDataAbertura(dataAbertura);
		
		response.sendRedirect("listaEmpresas");
	}

}
