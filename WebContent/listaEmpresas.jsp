<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List, br.com.jorge.gerenciamento.servlet.Empresa" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%--PARA JSTL FUNCIONAR, IMPORTAR BIBLIOTECA jstl-1.2.jar PARA ../gerenciamento/WebContent/WEB-INF/lib/jstl-1.2.jar --%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Lista de Empresas - Page JSP</title>
</head>
<body style="background-color:#000; color:#f9f9f9">
		
		<%-- VERIFICANDO SE EMPRESA ESTA NULO, OU SE NAO ESTA NULO ELE FAZ O METODO ABAIXO --%>
		<c:if test="${ not empty empresa }">
		<h2>Conclu�do..! Estado normal do servidor</h2>
		Empresa Cadastrada com sucesso => ${ empresa } <%-- EXPRESSION  LANGUAGE ${} SEMPRE TENTA INTERPRETAR E MOSTRAR O CONTEUDO --%>
		</c:if>
		
		<h2>Lista de Empresas</h2>
		<ul>
			<c:forEach items="${empresas}" var="empresa">
				<li>${ empresa.nome } - <fmt:formatDate value="${empresa.dataAbertura }" pattern="dd/MM/yyyy" /> => 
				<a href="/gerenciamento/removeEmpresa?id=${ empresa.id }" >Remove</a> - 
				<a href="/gerenciamento/mostrarEmpresa?id=${ empresa.id }" >Edita</a></li>
			</c:forEach>
		</ul>
		
</body>
</html>