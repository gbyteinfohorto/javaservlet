<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%--PARA JSTL FUNCIONAR, IMPORTAR BIBLIOTECA jstl-1.2.jar PARA ../gerenciamento/WebContent/WEB-INF/lib/jstl-1.2.jar --%>


<c:url value="/alterarEmpresa" var="linkServletNovaEmpresa"/>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Cadastrando Nova Empresa - Page HTML</title>
</head>
<body style="background-color: black;">


	<form action="${ linkServletNovaEmpresa }" method="POST"><!-- DEFININDO A URL REPASSADO PELO FORM NA AC�O DE SUBMIT -->
	
		<span style="color: #fff">Nome: <input type="text" name="nome" value="${ empresa.nome }"></span>
		
		<span style="color: #fff">Data Abertura: <input type="text" name="data" value="<fmt:formatDate value="${ empresa.dataAbertura }" pattern="dd/MM/yyyy" />"></span> 
		
		<span style="color: #fff"><%--Id da Empresa: --%>${ empresa.id }<input type="hidden" name="id" value="${ empresa.id }" style="background-color: #f3b5a8; color:#000"/></span> 
		
		
		<input type="submit" value="Alterar Dados"/>
	</form>
	
</body>
</html>